package com.alibaba.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.alibaba.domain.model.User
import com.alibaba.domain.usecase.GetUsersUseCase
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val getUsersUseCase = GetUsersUseCase()
    private val _usersList = MutableLiveData<List<User>>()
    val usersList: LiveData<List<User>> = _usersList

    private val _usersRequestException = MutableLiveData<Throwable>()
    val usersRequestException: LiveData<Throwable> = _usersRequestException

    init {
        viewModelScope.launch {
            loadUsers()
        }
    }

    private suspend fun loadUsers() {
        getUsersUseCase(Unit)
            .onSuccess {
                _usersList.value = it
            }
            .onFailure {
                _usersRequestException.value = it
            }
    }
}