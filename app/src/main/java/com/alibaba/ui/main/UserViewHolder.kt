package com.alibaba.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.alibaba.R
import com.alibaba.databinding.ItemUserBinding
import com.alibaba.domain.model.User

class UserViewHolder private constructor(
    private var binding: ItemUserBinding
) : ViewHolder(binding.root) {

    fun bind(user: User) = with(binding) {
        textViewItemUserName.text =
            root.context.getString(R.string.item_user_display_name_format, user.firstName, user.lastName)
        textViewItemUserEmail.text = user.email
        imageViewItemUserAvatar.load(user.avatar) {
            crossfade(true)
            placeholder(R.drawable.place_holder)
            error(R.drawable.place_holder)
        }
    }

    companion object {
        fun createInstance(parent: ViewGroup): UserViewHolder {
            return ItemUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ).let { UserViewHolder(it) }
        }
    }
}