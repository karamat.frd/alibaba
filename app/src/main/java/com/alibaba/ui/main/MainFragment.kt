package com.alibaba.ui.main

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import by.kirich1409.viewbindingdelegate.viewBinding
import com.alibaba.R
import com.alibaba.databinding.FragmentMainBinding
import com.alibaba.databinding.SecondSectionMainFragmentBinding
import com.alibaba.ui.search.SearchFragment
import com.google.android.material.snackbar.BaseTransientBottomBar.LENGTH_SHORT
import com.google.android.material.snackbar.Snackbar

class MainFragment : Fragment(R.layout.fragment_main) {

    private val viewModel by viewModels<MainViewModel>()
    private val binding: FragmentMainBinding by viewBinding()
    private var adapter: UsersAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupSecondSection(binding.includeMainFragmentSecondSection)
        setupThirdSection(binding.recyclerViewMainFragment)
        observeViewModel()
        observeSearchFragmentResults()
    }

    private fun observeSearchFragmentResults() {
        setFragmentResultListener(SearchFragment.REQUEST_KEY) { _, bundle: Bundle ->
            val destination = bundle.getString(SearchFragment.DESTINATION)
            binding.includeMainFragmentSecondSection.editTextSecondSection.setText(destination)
        }
    }

    private fun setupSecondSection(secondSectionBinding: SecondSectionMainFragmentBinding) {
        with(secondSectionBinding) {
            editTextSecondSection.doAfterTextChanged {
                buttonSecondSection.isEnabled = it.isNullOrEmpty().not()
            }
            buttonSecondSection.setOnClickListener {
                navigateToSearchFragment(editTextSecondSection.text.toString())
            }
        }
    }

    private fun navigateToSearchFragment(destination: String) {
        findNavController().navigate(
            R.id.to_search_fragment,
            bundleOf(SearchFragment.DESTINATION to destination)
        )
    }

    private fun setupThirdSection(recyclerView: RecyclerView) {
        recyclerView.adapter = UsersAdapter().also {
            adapter = it
        }
    }

    private fun observeViewModel() {
        viewModel.usersList.observe(viewLifecycleOwner) {
            if (it != null)
                adapter?.submitList(it)
        }
        viewModel.usersRequestException.observe(viewLifecycleOwner) {
            if (it != null) {
                Snackbar.make(
                    binding.root,
                    R.string.main_fragment_user_request_failure,
                    LENGTH_SHORT
                ).show()
            }
        }
    }

    override fun onDestroyView() {
        adapter = null
        super.onDestroyView()
    }
}