package com.alibaba.ui.search

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.navigation.fragment.findNavController
import by.kirich1409.viewbindingdelegate.viewBinding
import com.alibaba.R
import com.alibaba.databinding.FragmentSearchBinding

class SearchFragment : Fragment(R.layout.fragment_search) {

    private var dateSetListener: OnDateSetListener? = null
    private val binding: FragmentSearchBinding by viewBinding()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() = with(binding) {
        editTextFragmentSearchFrom.doAfterTextChanged { updateSearchButtonState() }
        editTextFragmentSearchDestination.apply {
            setText(arguments?.getString(DESTINATION))
            doAfterTextChanged { updateSearchButtonState() }
        }
        buttonFragmentSearchChooseDate.setOnClickListener {
            openDatePicker()
        }
        textViewFragmentSearchDate.doAfterTextChanged { updateSearchButtonState() }
        buttonFragmentSearchSearch.setOnClickListener {
            returnResults(editTextFragmentSearchDestination.text.toString())
        }
    }

    private fun updateSearchButtonState() {
        binding.buttonFragmentSearchSearch.isEnabled = shouldSearchButtonBeEnabled()
    }

    private fun shouldSearchButtonBeEnabled(): Boolean = with(binding) {
        editTextFragmentSearchFrom.text.isNullOrEmpty().not() &&
                editTextFragmentSearchDestination.text.isNullOrEmpty().not() &&
                textViewFragmentSearchDate.text.isNullOrEmpty().not()
    }

    private fun returnResults(destination: String) {
        setFragmentResult(REQUEST_KEY, bundleOf(DESTINATION to destination))
        findNavController().popBackStack()
    }

    private fun openDatePicker() {
        DatePickerDialog(
            requireContext(),
            createOnDateSetListener().also { dateSetListener = it },
            2022,
            10,
            5
        ).show()
    }

    private fun createOnDateSetListener() = OnDateSetListener { _, year, month, dayOfMonth ->
        binding.textViewFragmentSearchDate.text =
            getString(R.string.search_fragment_date_format, year, month, dayOfMonth)
    }

    override fun onDestroyView() {
        dateSetListener = null
        super.onDestroyView()
    }

    companion object {
        const val DESTINATION = "destination"
        const val REQUEST_KEY = "request_key"
    }
}