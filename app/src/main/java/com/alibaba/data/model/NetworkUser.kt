package com.alibaba.data.model

data class NetworkUser(
    val id: String?,
    val email: String?,
    val first_name: String?,
    val last_name: String?,
    val avatar: String?
)