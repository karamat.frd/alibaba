package com.alibaba.data.model.mapper

import com.alibaba.data.model.NetworkUser
import com.alibaba.domain.model.User

class UserMapper : Mapper<NetworkUser, User> {
    override fun map(input: NetworkUser): User {
        return with(input) {
            User(
                id = id.orEmpty(),
                email = email.orEmpty(),
                firstName = first_name.orEmpty(),
                lastName = last_name.orEmpty(),
                avatar = avatar.orEmpty(),
            )
        }
    }
}