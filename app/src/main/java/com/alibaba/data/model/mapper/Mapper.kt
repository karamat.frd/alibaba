package com.alibaba.data.model.mapper

interface Mapper<IN, OUT> {
    fun map(input: IN): OUT
}