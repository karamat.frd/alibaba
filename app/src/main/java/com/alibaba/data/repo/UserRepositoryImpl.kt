package com.alibaba.data.repo

import com.alibaba.data.model.NetworkUser
import com.alibaba.data.model.mapper.Mapper
import com.alibaba.data.model.mapper.UserMapper
import com.alibaba.data.network.NetworkUserDataSource
import com.alibaba.domain.model.User
import com.alibaba.domain.repo.UserRepository

class UserRepositoryImpl : UserRepository {
    private val dataSource: NetworkUserDataSource = NetworkUserDataSource()
    private val userMapper: Mapper<NetworkUser, User> = UserMapper()

    override suspend fun getUsers(): Result<List<User>> {
        return dataSource.getUsers().fold(
            onSuccess = {
                Result.success(it.map(userMapper::map))
            },
            onFailure = {
                Result.failure(it)
            }
        )
    }
}

