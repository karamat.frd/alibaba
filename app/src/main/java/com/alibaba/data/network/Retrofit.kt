package com.alibaba.data.network

import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object Retrofit {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://reqres.in/")
        .addConverterFactory(MoshiConverterFactory.create())
        .build()
    val userService: UserService = retrofit.create(UserService::class.java)
}