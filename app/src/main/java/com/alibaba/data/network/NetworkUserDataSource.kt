package com.alibaba.data.network

import com.alibaba.data.model.NetworkUser

class NetworkUserDataSource {
    suspend fun getUsers(): Result<List<NetworkUser>> {
        return try {
            val service = Retrofit.userService
            Result.success(service.getUsers().data ?: emptyList())
        } catch (ex: Exception) {
            Result.failure(ex)
        }
    }
}