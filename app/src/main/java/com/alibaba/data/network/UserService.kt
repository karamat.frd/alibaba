package com.alibaba.data.network

import com.alibaba.data.model.NetworkUserWrapper
import retrofit2.http.GET

interface UserService {
    @GET("api/users")
    suspend fun getUsers(): NetworkUserWrapper
}