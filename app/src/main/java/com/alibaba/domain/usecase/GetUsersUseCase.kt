package com.alibaba.domain.usecase

import com.alibaba.domain.model.User
import com.alibaba.data.repo.UserRepositoryImpl
import com.alibaba.domain.repo.UserRepository

class GetUsersUseCase : UseCase<List<User>, Unit>() {
    private val userRepositoryImpl: UserRepository = UserRepositoryImpl()

    override suspend fun run(params: Unit): Result<List<User>> {
        return userRepositoryImpl.getUsers()
    }
}