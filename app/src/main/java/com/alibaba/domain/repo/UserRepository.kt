package com.alibaba.domain.repo

import com.alibaba.domain.model.User

interface UserRepository {
    suspend fun getUsers(): Result<List<User>>
}